import logging
import argparse
import settings
import gitlab


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--debug", "--verbose", "-d", "-v",
        help="more output",
        dest='debug',
        action='store_true',
        default=False,
    )

    # not implemented
    parser.add_argument(
        "--test", "--dry-run", "-t",
        help="dont write to db",
        dest='test_mode',
        action='store_true',
        default=False,
    )

    parser.add_argument(
        "--username", "-u",
        dest='username',
        help="specify the username of th gitlab user you want to remove",
    )

    args = parser.parse_args()

    log_level = logging.INFO

    if args.debug:
        log_level = logging.DEBUG

    # this logging setup can then be used anywhere downstream as:
    # logging.debug() or logging.info(), for example
    logging.basicConfig(level=log_level)

    # console
    console = logging.StreamHandler()
    console.setLevel(log_level)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    if not logging.getLogger('').handlers:
        logging.getLogger('').addHandler(console)

    if args.test_mode:
        print('TEST MODE is ON')

    if not args.username:
        parser.error('you must specify a username')

    gl = gitlab.Gitlab(
        settings.GITLAB_INSTANCE,
        settings.GITLAB_TOKEN,
        api_version="4",
        timeout=settings.GITLAB_TIMEOUT,
    )
    gl.auth()

    # we need maintainer access (40) in order to remove users
    projects = gl.projects.list(all=True, membership=True, min_access_level=30, as_list=False)

    for project in projects:
        if args.debug:
            print('trying project {}'.format(project.name))
        for member in project.members.list():
            if member.username == args.username:
                print('deleting user {} from project {}'.format(
                    member.username,
                    project.name,
                ))
                if args.test_mode:
                    print('TEST MODE - not really deleting.')
                else:
                    member.delete()


if __name__ == '__main__':
    # in here belongs code that should only be executed when the module is called directly
    # (i.e. fro command line like that: python main.py
    main()

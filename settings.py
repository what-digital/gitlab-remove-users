import os
from dotenv import load_dotenv

# Create .env file path.
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')

# Load file from the path.
load_dotenv(dotenv_path)

# a convenient shortcut to import environment variables
env = os.environ.get
true_values = ['1', 'true', 'y', 'yes', 'on', 1, True]

GITLAB_TOKEN = env('GITLAB_TOKEN', '123')
GITLAB_API_BASE_URL = "https://gitlab.com/api/v4/"
GITLAB_INSTANCE = 'https://gitlab.com'
GITLAB_TIMEOUT = 60

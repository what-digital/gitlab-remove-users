# Remove users from gitlab projects

- This removes users from all gitlab projects your token has maintainer access to.

## Setup
- copy .env-example to .env and fill in your gitlab token
- create a virtual environment
- install the pip requirements

## Run

- `python main.py --username=UserToBeRemoved`
- Test mode: `python main.py --test --username=UserToBeRemoved`
- Verbose / debug mode: `python main.py --test -d --username=UserToBeRemoved`
